package com.wavelabs.deal.model.entities.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.deal.model.events.ShowInterestHandler;
import com.wavelabs.deal.model.utilities.ObjectBuilder;

@RunWith(MockitoJUnitRunner.class)

public class ShowInterestHandlerTest {

	@InjectMocks
	ShowInterestHandler showInterestHandlerMock;
	ShowInterestHandler showInterestHandler;

	@Before
	public void setUp() {
		showInterestHandler = ObjectBuilder.getshowInterestHandler();
	}

	@Test
	public void testDefaultShowHandler() {
		showInterestHandlerMock.setDescription(showInterestHandler.getDescription());
		Assert.assertEquals(showInterestHandler.getDescription(), showInterestHandlerMock.getDescription());
		showInterestHandlerMock.setEventStatus(showInterestHandler.getEventStatus());
		Assert.assertEquals(showInterestHandler.getEventStatus(), showInterestHandlerMock.getEventStatus());
		showInterestHandlerMock.setEventType(showInterestHandler.getEventType());
		Assert.assertEquals(showInterestHandler.getEventType(), showInterestHandlerMock.getEventType());
		showInterestHandlerMock.setTimeStamp(showInterestHandler.getTimeStamp());
		Assert.assertEquals(showInterestHandler.getTimeStamp(), showInterestHandlerMock.getTimeStamp());
		showInterestHandlerMock.setId(showInterestHandler.getId());
		Assert.assertEquals(showInterestHandler.getId(), showInterestHandlerMock.getId());
	}

	@Test
	public void testId() {
		showInterestHandlerMock.setId(showInterestHandler.getId());
		Assert.assertEquals(showInterestHandler.getId(), showInterestHandlerMock.getId());
	}

	@Test
	public void testDescription() {
		showInterestHandlerMock.setDescription(showInterestHandler.getDescription());
		Assert.assertEquals(showInterestHandler.getDescription(), showInterestHandlerMock.getDescription());
	}

	@Test
	public void testEventStatus() {
		showInterestHandlerMock.setEventStatus(showInterestHandler.getEventStatus());
		Assert.assertEquals(showInterestHandler.getEventStatus(), showInterestHandlerMock.getEventStatus());
	}

	@Test
	public void testEventType() {
		showInterestHandlerMock.setEventType(showInterestHandler.getEventType());
		Assert.assertEquals(showInterestHandler.getEventType(), showInterestHandlerMock.getEventType());
	}

	@Test
	public void testEventTimeStamp() {
		showInterestHandlerMock.setTimeStamp(showInterestHandler.getTimeStamp());
		Assert.assertEquals(showInterestHandler.getTimeStamp(), showInterestHandlerMock.getTimeStamp());
	}

}
