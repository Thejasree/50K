package com.wavelabs.deal.model.entities.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.model.utilities.ObjectBuilder;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {
	@InjectMocks
	User userMock;
	User user;

	@Before
	public void setUp() {
		user = ObjectBuilder.getUser();
	}

	@Test
	public void testDefaultConstructor() {
		userMock.setEmail(user.getEmail());
		Assert.assertEquals(user.getEmail(), userMock.getEmail());
		userMock.setName(user.getName());
		Assert.assertEquals(user.getName(), userMock.getName());
		userMock.setUuid(user.getUuid());
		Assert.assertEquals(user.getUuid(), userMock.getUuid());
		userMock.setImage(user.getImage());
		Assert.assertEquals(user.getImage(), userMock.getImage());
		userMock.setId(user.getId());
		Assert.assertEquals(user.getId(), userMock.getId());

	}

	@Test
	public void testId() {
		userMock.setId(user.getId());
		Assert.assertEquals(user.getId(), userMock.getId());
	}

	@Test
	public void testName() {
		userMock.setName(user.getName());
		Assert.assertEquals(user.getName(), userMock.getName());
	}

	@Test
	public void testEmail() {
		userMock.setEmail(user.getEmail());
		Assert.assertEquals(user.getEmail(), userMock.getEmail());
	}

	@Test
	public void testUuid() {
		userMock.setUuid(user.getUuid());
		Assert.assertEquals(user.getUuid(), userMock.getUuid());

	}

	@Test
	public void testImage() {

		userMock.setImage(user.getImage());
		Assert.assertEquals(user.getImage(), userMock.getImage());

	}
}
