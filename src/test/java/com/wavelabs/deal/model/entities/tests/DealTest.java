package com.wavelabs.deal.model.entities.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.model.utilities.ObjectBuilder;

@RunWith(MockitoJUnitRunner.class)
public class DealTest {
	@InjectMocks
	Deal dealMock;
	Deal deal;

	@Before
	public void setUp() {
		deal = ObjectBuilder.getDeal();
	}

	@Test
	public void testDefaultDeal() {
		dealMock.setId(deal.getId());
		Assert.assertEquals(deal.getId(), dealMock.getId());
		dealMock.setUuid(deal.getUuid());
		Assert.assertEquals(deal.getUuid(), dealMock.getUuid());
		dealMock.setSecuredFunding(deal.getSecuredFunding());
		Assert.assertEquals(deal.getSecuredFunding(), dealMock.getSecuredFunding());
	}

	@Test
	public void testUuid() {
		dealMock.setUuid(deal.getUuid());
		Assert.assertEquals(deal.getUuid(), dealMock.getUuid());
	}

	@Test
	public void testFundingAmount() {
		dealMock.setSecuredFunding(deal.getSecuredFunding());
		Assert.assertEquals(deal.getSecuredFunding(), dealMock.getSecuredFunding());
	}

	@Test
	public void testId() {
		dealMock.setId(deal.getId());
		Assert.assertEquals(deal.getId(), dealMock.getId());
	}
}
