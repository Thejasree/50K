package com.wavelabs.deal.domain.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author thejasreem LoginCommandController is for getting a user profile from
 *         Linkedin. After creating application, you will get Client_ID and
 *         Client_Secret. Using that you will get authorization code. From the
 *         authorization code you will get the access_token. Using that get the
 *         profile from Linkedin.
 */

@RestController
@Component
public class LoginCommandController {
	static final Logger log = Logger.getLogger(LoginCommandController.class);

}
