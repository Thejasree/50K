package com.wavelabs.deal.domain.controllers;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.event.service.ShowInterestEventService;
import com.wavelabs.deal.model.event.service.ShowInterestFactory;
import com.wavelabs.deal.model.events.EventStatus;
import com.wavelabs.deal.model.events.EventType;
import com.wavelabs.deal.model.events.ShowInterestHandler;
import com.wavelabs.deal.model.service.ShowInterestService;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem InvestmentCommandController is the controller class.
 *         showInterestOnDeal() method is for searching the people who has shown
 *         interest on a particular deal. If any one shows interest on a deal,
 *         admin receives an email.(User shows interest on a deal)
 */
@RestController
@Component
public class ShowInterestCommandController {
	static final Logger log = Logger.getLogger(ShowInterestCommandController.class);
	@Autowired
	ShowInterestService interestService;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	JavaMailSender sender;
	@Autowired
	ShowInterestFactory showInterestEventFactory;
	@Autowired
	ShowInterestEventService eventService;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/deal/{dealUuid}/interest/{userUuid}", method = RequestMethod.POST)
	public ResponseEntity showInterestOnDeal(@RequestParam String dealUuid, @RequestParam String userUuid) {
		RestMessage restMessage = new RestMessage();
		Deal flag = interestService.showInterestOnDeal(dealUuid, userUuid);
		if (flag != null) {

			ShowInterestHandler event = showInterestEventFactory.showInterestOnDealEvent(EventType.DealEvents,
					EventStatus.success);
			eventService.createEventForShowInterest(event);

			restMessage.messageCode = "200";
			restMessage.message = "User shown interest  successfull!";
			try {
				User user = userRepo.findByUuid(userUuid);
				String username = user.getName();
				sendEmail(username);
			} catch (Exception e) {
				log.info(e);
			}
			return ResponseEntity.status(200).body(restMessage);
		} else {

			ShowInterestHandler event = showInterestEventFactory.showInterestOnDealEventFail(EventType.DealEvents,
					EventStatus.fail);
			eventService.createEventForShowInterest(event);

			restMessage.messageCode = "404";
			restMessage.message = "Investment unsuccessfull!";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

	/**
	 * sendEmail() method sends an email to Admin about the people who are
	 * interested on deal.
	 * 
	 * @param username
	 * @param dealname
	 * @throws MessagingException
	 */
	private void sendEmail(String username) throws MessagingException {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setTo("mthejasree925@gmail.com");
		helper.setText("Hi " + username + " shown interest on a deal.");
		helper.setSubject(username +" shown interest on a deal.");
		sender.send(message);
	}

}