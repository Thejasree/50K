package com.wavelabs.deal.domain.controllers;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Component
public class RedirectURLQueryController {
	static final Logger log = Logger.getLogger(RedirectURLQueryController.class);

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/linkedin")
	public ResponseEntity getLinkdeinURL() {
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();

	}

}
