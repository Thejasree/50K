package com.wavelabs.deal.domain.controllers;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem DealQueryController is a class. getAllDeals() method
 *         retrieves all the deals from the uri(i.e from content service).
 * 
 */

@RestController
@Component
public class DealQueryController {
	static final Logger log = Logger.getLogger(DealQueryController.class);
	@Autowired
	private Environment env;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	InvestmentRepository investRepo;

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/getAllDeals")
	public ResponseEntity getAllDeals() {
		final String uri = env.getProperty("dealUrl");
		RestTemplate restTemplate = new RestTemplate();
		RestMessage restMessage = new RestMessage();
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		if (response != null) {
			return ResponseEntity.status(200).body(response);
		} else {
			restMessage.message = "404";
			restMessage.messageCode = "Retrieval of Deals Fails!!";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	/**
	 * getDealByUuid() method is for getting a particular deal using deal uuid.
	 * Bind the securedFunding, user deatils,amount to the content response
	 * object. if the given deal uuid doesn't exists in database return values
	 * as null ,otherwise add all existing values and bind it to response
	 * object.
	 * 
	 * @param dealUuid
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(method = RequestMethod.GET, value = "/deal")
	public ResponseEntity getDealById(@RequestParam String dealId) {
		String uri = env.getProperty("dealIdUrl") + dealId;
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		String jsonString = response.getBody();

		JSONParser jParser = new JSONParser();
		JSONObject jObject = null;
		Investment[] investment = null;
		// parsing data
		try {
			jObject = (JSONObject) (jParser.parse(jsonString));
		} catch (ParseException e) {
			log.error(e);
		}
		String dUuid = (String) jObject.get("id");

		// querying the DB to chek if the UUID exists
		Deal dealObject = dealRepo.findByUuid(dUuid);
		// if the uuid exists
		if (dealObject != null) {
			Double amount = dealObject.getSecuredFunding();
			jObject.put("securedFunding", amount);
			investment = investRepo.findByDeal(dealObject);
			// creating a new JSONArray for peopleInterested
			JSONArray peopleInterested = new JSONArray();
			// setting user and amount as key value pairs to JSONOject.
			for (Investment in1 : investment) {
				JSONObject profile = new JSONObject();
				profile.put("user", in1.getUser());
				profile.put("amount", in1.getAmount());
				peopleInterested.add(profile);
			}
			if (peopleInterested.isEmpty()) {
				jObject.put("peopleInterested", null);
			} else {
				jObject.put("peopleInterested", peopleInterested);
			}
		} else {
			// if the uuid doesn't exist
			jObject.put("peopleInterested", null);
			jObject.put("securedFunding", 0.0);
		}
		return ResponseEntity.status(200).body(jObject);
	}
}
