package com.wavelabs.deal.domain.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.model.service.UserService;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
@RequestMapping(value = "/user")
public class UserCommandController {
	@Autowired
	UserService userService;
	@Autowired
	RestMessage restMessage;

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity createNewUser(@RequestBody User user) {
		Boolean flag = userService.createNewUser(user);
		if (flag) {
			
			restMessage.messageCode = "200";
			restMessage.message = "User creation success!";
			return ResponseEntity.status(200).body(restMessage);
		} else {
			
			restMessage.messageCode = "404";
			restMessage.message = "User creation fail!";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

}
