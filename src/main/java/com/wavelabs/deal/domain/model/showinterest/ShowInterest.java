package com.wavelabs.deal.domain.model.showinterest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * @author thejasreem ShowInterest is a class which shows the user interest for
 *         the particular deal.
 * 
 */
@Entity
public class ShowInterest {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int userUuid;
	private String username;
	private String dealUuid;

	public ShowInterest() {

	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(int userId) {
		this.userUuid = userId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setDealUuid(String dealUuid) {
		this.dealUuid = dealUuid;
	}

	public int getId() {
		return id;
	}

	public int getUserId() {
		return userUuid;
	}

	public String getUsername() {
		return username;
	}

	public String getDealUuid() {
		return dealUuid;
	}

}
