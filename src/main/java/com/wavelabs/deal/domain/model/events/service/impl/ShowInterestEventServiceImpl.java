package com.wavelabs.deal.domain.model.events.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.deal.domain.repositories.ShowInterestEventRepository;
import com.wavelabs.deal.domain.repositories.ShowInterestEventRepository;
import com.wavelabs.deal.model.event.service.ShowInterestEventService;
import com.wavelabs.deal.model.events.ShowInterestHandler;

/**
 * 
 * @author thejasreem ShowInterestEventServiceImpl implementation class.
 *         createEventForShowInterest() method for storing event in database.
 *         Returns true when it is sucess otherwise false.
 */
@Service
public class ShowInterestEventServiceImpl implements ShowInterestEventService {
	static Logger log = Logger.getLogger(ShowInterestEventServiceImpl.class);
	@Autowired
	ShowInterestEventRepository interestEventRepo;

	@Override
	public boolean createEventForShowInterest(ShowInterestHandler handler) {
		try {
			interestEventRepo.save(handler);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}
}
