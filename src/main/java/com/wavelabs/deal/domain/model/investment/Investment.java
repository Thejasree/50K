package com.wavelabs.deal.domain.model.investment;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.user.User;

/**
 * 
 * @author thejasreem Investment is a pojo class. Parameters: id, amount,
 *         userId, dealId. It has stters and getters
 */
@Entity
public class Investment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private Double amount;
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH })
	private User user;
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REFRESH })
	private Deal deal;

	public Investment() {

	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setDeal(Deal deal) {
		this.deal = deal;
	}

	public int getId() {
		return id;
	}

	public Double getAmount() {
		return amount;
	}

	public User getUser() {
		return user;
	}

	public Deal getDeal() {
		return deal;
	}

}
