package com.wavelabs.deal.model.utilities;

import java.util.Calendar;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.model.showinterest.ShowInterest;
import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.model.events.EventStatus;
import com.wavelabs.deal.model.events.EventType;
import com.wavelabs.deal.model.events.ShowInterestHandler;

public class ObjectBuilder {
	public static User getUser() {
		User user = new User();
		user.setEmail("anu@gmail.com");
		user.setName("ANU");
		user.setUuid("abc1234");
		user.setImage("abckd");
		return user;

	}

	public static Deal getDeal() {
		Deal deal = new Deal();
		deal.setSecuredFunding(0.0);
		deal.setUuid("ab123");
		return deal;

	}

	public static Investment getInvestment() {
		Investment invest = new Investment();
		invest.setAmount(1000.00);
		invest.setDeal(getDeal());
		invest.setUser(getUser());
		return invest;

	}

	public static ShowInterest getShowInterest() {
		ShowInterest interest = new ShowInterest();
		interest.setUserId(2);
		interest.setUsername("anu");
		interest.setDealUuid("abc12345");
		return interest;
	}

	public static ShowInterestHandler getshowInterestHandler() {
		ShowInterestHandler showInterestHandler = new ShowInterestHandler();
		showInterestHandler.setDescription("ahown interest ");
		showInterestHandler.setEventStatus(EventStatus.success);
		showInterestHandler.setEventType(EventType.DealEvents);
		showInterestHandler.setTimeStamp(Calendar.getInstance());
		return showInterestHandler;
	}

	public static Deal getDeal1() {
		Deal deal1 = new Deal();
		deal1.setSecuredFunding(0.0);
		deal1.setUuid("ab12");
		return deal1;
	}
}
