package com.wavelabs.deal.model.service;

import com.wavelabs.deal.domain.model.user.User;
/**
 * 
 * @author thejasreem
 * UserService is for service call.
 * createNewUser is the method for creating new User.
 * @Object User
 */
public interface UserService {
public boolean createNewUser(User user);
}
