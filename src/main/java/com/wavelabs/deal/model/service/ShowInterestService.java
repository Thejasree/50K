package com.wavelabs.deal.model.service;

import com.wavelabs.deal.domain.model.deal.Deal;

public interface ShowInterestService {


	Deal showInterestOnDeal(String dealUuid, String userUuid);

}
