package com.wavelabs.deal.model.service;

import com.wavelabs.deal.domain.model.investment.Investment;

public interface InvestmentService {


	Investment processInvestAmount(String dealUuid, String userUuid, double amount);




}
