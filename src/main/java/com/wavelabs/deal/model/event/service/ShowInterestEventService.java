package com.wavelabs.deal.model.event.service;

import com.wavelabs.deal.model.events.ShowInterestHandler;

/**
 * 
 * @author thejasreem ShowInterestEventService is an interface.
 *         createEventForShowInterest() method call for creating an event for
 *         showing interest on particular deal.
 */
public interface ShowInterestEventService {
	boolean createEventForShowInterest(ShowInterestHandler handler);
}
