package com.wavelabs.deal.model.event.service;

import java.util.Calendar;

import org.springframework.stereotype.Component;

import com.wavelabs.deal.model.events.EventStatus;
import com.wavelabs.deal.model.events.EventType;
import com.wavelabs.deal.model.events.ShowInterestHandler;

/**
 * 
 * @author thejasreem showInterestOnDealEvent() method is for success event when
 *         the user shows interest on deal.
 * @param type
 * @param status
 * @return
 */
@Component
public class ShowInterestFactory {
	public ShowInterestHandler showInterestOnDealEvent(EventType type, EventStatus status) {
		ShowInterestHandler event = new ShowInterestHandler();
		event.setTimeStamp(Calendar.getInstance());
		event.setEventStatus(EventStatus.success);
		event.setEventType(EventType.DealEvents);
		event.setDescription("Showed interest on deal!");
		return event;

	}

	/**
	 * showInterestOnDealEventFail() method is for failure event when the user
	 * shows interest on deal.
	 * 
	 * @param type
	 * @param status
	 * @return
	 */
	public ShowInterestHandler showInterestOnDealEventFail(EventType type, EventStatus status) {
		ShowInterestHandler event = new ShowInterestHandler();
		event.setTimeStamp(Calendar.getInstance());
		event.setEventStatus(EventStatus.fail);
		event.setEventType(EventType.DealEvents);
		event.setDescription("Showed interest on deal fails!");
		return event;

	}
}
