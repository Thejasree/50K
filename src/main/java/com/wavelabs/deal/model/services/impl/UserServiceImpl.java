package com.wavelabs.deal.model.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	static Logger log = Logger.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepo;

	@Override
	public boolean createNewUser(User user) {
		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

}
