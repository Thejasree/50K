package com.wavelabs.deal.model.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.service.InvestmentService;

@Service
public class InvestmentServiceImpl implements InvestmentService {

	static Logger log = Logger.getLogger(InvestmentServiceImpl.class);
	@Autowired
	InvestmentRepository investRepo;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;

	/**
	 * processInvestAmount() method is for adding Amount based on deals. Store
	 * the amount, dealId, userId on Investment object. Get the total amount by
	 * using dealId. Update the securedFunding with that total amount which is
	 * in Deal Object.
	 */

	public Investment processInvestAmount(String dealUuid, String userUuid, double amount) {
		Investment investment = new Investment();
		Deal deal1 = new Deal();
		Deal deal = dealRepo.findByUuid(dealUuid);
		if (deal == null) {
			deal1.setSecuredFunding(0.0);
			deal1.setUuid(dealUuid);
			dealRepo.save(deal1);
		} 
			investment.setAmount(amount);
			Deal dealObj = dealRepo.findByUuid(dealUuid);
			User user = userRepo.findByUuid(userUuid);
			investment.setUser(user);
			investment.setDeal(dealObj);
			investRepo.save(investment);
			Double dealAmount = dealRepo.findSecuredFundingById(dealUuid);
			dealRepo.updateSecuredFundingByUuid(dealAmount, dealUuid);
			return investment;
		
	}

	/**
	 * getSecuredFundingByUuid() method is for getting securedFunding on a deal.
	 * It returns total secured funding amount.
	 *//*
	@Override
	public Double getSecuredFunding(String dealUuid) {
		Deal deal = dealRepo.findByUuid(dealUuid);
		return deal.getSecuredFunding();
	}*/

}
