package com.wavelabs.deal.model.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.service.ShowInterestService;

/**
 * 
 * @author thejasreem
 *
 */
@Service
public class ShowInterestServiceImpl implements ShowInterestService {
	static final Logger log = Logger.getLogger(ShowInterestServiceImpl.class);
	@Autowired
	InvestmentRepository investRepo;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;

	/**
	 * showInterestOnDeal() method is for the people who shows interest on deal.
	 * if the given deal uuid doesn't exists, make a new entry in deal.
	 * 
	 * @return invest
	 */

	@Override
	public Deal showInterestOnDeal(String dealUuid, String userUuid) {
		Deal deal1 = new Deal();
		try {
			Deal deal = dealRepo.findByUuid(dealUuid);
			if (deal == null) {

				deal1.setSecuredFunding(0.0);
				deal1.setUuid(dealUuid);
				dealRepo.save(deal1);
			}
		} catch (Exception e) {
			log.error(e);
			return null;
		}
		return deal1;

	}

}
