package com.wavelabs.deal.model.events;
/**
 * 
 * @author thejasreem
 * EventStatus is an enum.
 * It is used to define status of an events.
 * Status is true or false.
 */
public enum EventStatus {
success, fail
}
