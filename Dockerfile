FROM java:8
ADD target/dealservice.jar dealservice.jar
ENTRYPOINT ["java","-jar","dealservice.jar"]
